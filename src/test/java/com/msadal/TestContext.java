package com.msadal;

import com.msadal.geo.DistanceMatrixProvider;
import com.msadal.geo.GoogleDistanceProvider;
import com.msadal.geo.HaversineDistanceProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestContext {

    @Bean
    public DistanceMatrixProvider haversineDistanceProvider() {
        return new HaversineDistanceProvider();
    }

    @Bean
    public DistanceMatrixProvider googleDistanceProvider() {
        return new GoogleDistanceProvider();
    }
}
