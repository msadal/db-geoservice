package com.msadal;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.msadal.domain.Shop;
import com.msadal.domain.ShopInfo;
import com.msadal.domain.ShopRegisterTask;
import com.msadal.domain.Status;
import com.msadal.geo.Coordinates;
import com.msadal.util.AbstractKeyFactory;
import com.msadal.util.LatLngValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = GeoServiceApp.class)
@WebAppConfiguration()
@IntegrationTest({"server.port:8090"})
public class ShopResourceControllerTest {
    @Value("${local.server.port}")
    int port;

    @Autowired
    private ShopResourceController restController;

    @Mock
    private ShopManager shopManagerMock;

    @Autowired
    private AbstractKeyFactory sequenceKeyFactory;

    @Autowired
    private AbstractKeyFactory md5HashKeyFactory;

    @Before
    public void setUp() throws Exception {
        RestAssured.port = port;
        MockitoAnnotations.initMocks(this);
        mockControllerShopManger();
    }

    private void mockControllerShopManger() throws Exception {
        Field field = ReflectionUtils.findField(ShopResourceController.class, "shopManager");
        field.setAccessible(true);
        ReflectionUtils.setField(field, restController, shopManagerMock);
    }

    @Test
    public void testRegister() throws Exception {
        String shopJson = "{ \"shopName\": \"Tesco Express\", " +
                "  \"shopAddress\": { \"number\": \"102\", \"postCode\": \"SE9 6UF\"}}";
        ObjectMapper mapper = new ObjectMapper();
        ShopInfo shopInfo = mapper.readValue(shopJson, ShopInfo.class);

        reset(shopManagerMock);
        when(shopManagerMock.addRegisterTask(any()))
                .thenReturn(new ShopRegisterTask(sequenceKeyFactory, shopInfo, Status.NEW));

        given()
                .contentType(ContentType.JSON)
                .body(shopJson)
                .when()
                .post("/register")
                .then()
                .statusCode(HttpStatus.OK.value())
                .body("key", notNullValue())
                .body("shopName", equalTo("Tesco Express"))
                .body("shopAddress.number", equalTo("102"))
                .body("shopAddress.postCode", equalTo("SE9 6UF"))
                .body("status", equalTo(Status.NEW.name()));

        verify(shopManagerMock, times(1)).addRegisterTask(any());
        verifyNoMoreInteractions(shopManagerMock);
    }

    @Test
    public void testRegisterEmptyNumber() throws Exception {
        String shopJson = "{ \"shopName\": \"Tesco Express\", " +
                "  \"shopAddress\": { \"number\": \"\", \"postCode\": \"SE9 6UF\"}}";

        reset(shopManagerMock);
        given()
                .contentType(ContentType.JSON)
                .body(shopJson)
                .when()
                .post("/register")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .body("key", nullValue())
                .body("shopName", equalTo("Tesco Express"))
                .body("shopAddress.number", equalTo(""))
                .body("shopAddress.postCode", equalTo("SE9 6UF"))
                .body("status", nullValue());

        verifyZeroInteractions(shopManagerMock);
    }

    @Test
    public void testRegisterNullNumber() throws Exception {
        String shopJson = "{ \"shopName\": \"Tesco Express\", " +
                "  \"shopAddress\": { \"postCode\": \"SE9 6UF\"}}";
        reset(shopManagerMock);
        given()
                .contentType(ContentType.JSON)
                .body(shopJson)
                .when()
                .post("/register")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .body("key", nullValue())
                .body("shopName", equalTo("Tesco Express"))
                .body("shopAddress.number", nullValue())
                .body("shopAddress.postCode", equalTo("SE9 6UF"));

        verifyZeroInteractions(shopManagerMock);
    }

    @Test
    public void testRegisterEmptyPostCode() throws Exception {
        String shopJson = "{ \"shopName\": \"Tesco Express\", " +
                "  \"shopAddress\": { \"number\": \"100\", \"postCode\": \"\"}}";
        reset(shopManagerMock);
        given()
                .contentType(ContentType.JSON)
                .body(shopJson)
                .when()
                .post("/register")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .body("key", nullValue())
                .body("shopName", equalTo("Tesco Express"))
                .body("shopAddress.number", equalTo("100"))
                .body("shopAddress.postCode", equalTo(""));
        verifyZeroInteractions(shopManagerMock);
    }

    @Test
    public void testRegisterNullPostCode() throws Exception {
        String shopJson = "{ \"shopName\": \"Tesco Express\", " +
                "  \"shopAddress\": { \"number\": \"100\"}}";
        reset(shopManagerMock);
        given()
                .contentType(ContentType.JSON)
                .body(shopJson)
                .when()
                .post("/register")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .body("key", nullValue())
                .body("shopName", equalTo("Tesco Express"))
                .body("shopAddress.number", equalTo("100"))
                .body("shopAddress.postCode", nullValue());
        verifyZeroInteractions(shopManagerMock);
    }

    @Test
    public void testRegisterNullAddress() throws Exception {
        String shopJson = "{ \"shopName\": \"Tesco Express\"}";
        reset(shopManagerMock);
        given()
                .contentType(ContentType.JSON)
                .body(shopJson)
                .when()
                .post("/register")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .body("key", nullValue())
                .body("shopName", equalTo("Tesco Express"))
                .body("shopAddress", nullValue());
        verifyZeroInteractions(shopManagerMock);
    }

    @Test
    public void testRegisterEmptyJSON() throws Exception {
        reset(shopManagerMock);
        given()
                .contentType(ContentType.JSON)
                .body("{}")
                .when()
                .post("/register")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .body("key", nullValue())
                .body("shopName", nullValue())
                .body("shopAddress", nullValue());
        verifyZeroInteractions(shopManagerMock);
    }

    @Test
    public void testGetTaskOK() throws Exception {
        String shopJson = "{ \"shopName\": \"Tesco Express\", " +
                "  \"shopAddress\": { \"number\": \"300\", \"postCode\": \"SE9 6UF\"}}";
        ObjectMapper mapper = new ObjectMapper();
        ShopInfo shopInfo = mapper.readValue(shopJson, ShopInfo.class);

        reset(shopManagerMock);
        when(shopManagerMock.getRegisterTask("1"))
                .thenReturn(new ShopRegisterTask(sequenceKeyFactory, shopInfo, Status.OK));
        given()
                .contentType(ContentType.JSON)
                .when()
                .get("/task/1")
                .then()
                .statusCode(HttpStatus.OK.value())
                .body("key", notNullValue())
                .body("shopName", equalTo("Tesco Express"))
                .body("shopAddress.number", equalTo("300"))
                .body("shopAddress.postCode", equalTo("SE9 6UF"))
                .body("status", equalTo(Status.OK.name()));

        verify(shopManagerMock, times(1)).getRegisterTask("1");
        verifyNoMoreInteractions(shopManagerMock);
    }

    @Test
    public void testGetTaskNotExist() throws Exception {

        reset(shopManagerMock);
        when(shopManagerMock.getRegisterTask("2"))
                .thenReturn(null);
        String response = given()
                .contentType(ContentType.JSON)
                .when()
                .get("/task/2")
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value())
                .extract().asString();

        verify(shopManagerMock, times(1)).getRegisterTask("2");
        verifyNoMoreInteractions(shopManagerMock);
        assertEquals("", response);
    }

    @Test
    public void testSearchShop() throws Exception {
        String shopJson = "{ \"shopName\": \"Boots\", " +
                "  \"shopAddress\": { \"number\": \"100\", \"postCode\": \"SE9 6UF\"}}";
        ObjectMapper mapper = new ObjectMapper();
        ShopInfo shopInfo = mapper.readValue(shopJson, ShopInfo.class);

        String lat = "66.1";
        String lng = "34.45";
        Coordinates coordinates = LatLngValidator.coordinates(lat, lng);
        reset(shopManagerMock);
        when(shopManagerMock.searchShop(coordinates))
                .thenReturn(new Shop(md5HashKeyFactory, shopInfo, 34.55d, 66.23d));

        given()
                .param("lat", lat).param("lng", lng)
                .when()
                .get("/search")
                .then()
                .statusCode(HttpStatus.OK.value())
                .body("key", notNullValue())
                .body("shopName", equalTo("Boots"))
                .body("shopAddress.number", equalTo("100"))
                .body("shopAddress.postCode", equalTo("SE9 6UF"))
                .body("shopLatitude", equalTo(66.23f))
                .body("shopLongitude", equalTo(34.55f));

        verify(shopManagerMock, times(1)).searchShop(any());
        verifyNoMoreInteractions(shopManagerMock);
    }

    @Test
    public void testSearchShopEmptyLatLng() throws Exception {
        String lat = "";
        String lng = null;
        Coordinates coordinates = LatLngValidator.coordinates(lat, lng);
        reset(shopManagerMock);

        String response = given()
                .param("lat", lat).param("lng", lng)
                .when()
                .get("/search")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .extract().asString();

        assertEquals("", response);
        verifyZeroInteractions(shopManagerMock);
    }

    @Test
    public void testSearchShopLatLngOutOfRange() throws Exception {
        String lat = "91.1";
        String lng = "181.34";
        Coordinates coordinates = LatLngValidator.coordinates(lat, lng);
        reset(shopManagerMock);

        String response = given()
                .param("lat", lat).param("lng", lng)
                .when()
                .get("/search")
                .then()
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .extract().asString();

        assertEquals("", response);
        verifyZeroInteractions(shopManagerMock);
    }

    @Test
    public void testSearchShopNotFound() throws Exception {
        String shopJson = "{ \"shopName\": \"Boots\", " +
                "  \"shopAddress\": { \"number\": \"100\", \"postCode\": \"SE9 6UF\"}}";
        ObjectMapper mapper = new ObjectMapper();
        ShopInfo shopInfo = mapper.readValue(shopJson, ShopInfo.class);

        String lat = "63.1";
        String lng = "32.45";
        Coordinates coordinates = LatLngValidator.coordinates(lat, lng);
        reset(shopManagerMock);
        when(shopManagerMock.searchShop(coordinates))
                .thenReturn(null);

        String response = given()
                .param("lat", lat).param("lng", lng)
                .when()
                .get("/search")
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value())
                .extract()
                .asString();

        assertEquals("", response);
        verify(shopManagerMock, times(1)).searchShop(any());
        verifyNoMoreInteractions(shopManagerMock);

    }
}