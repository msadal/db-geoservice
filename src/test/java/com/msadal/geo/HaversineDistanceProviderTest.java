package com.msadal.geo;

import com.msadal.GeoServiceApp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = GeoServiceApp.class)
public class HaversineDistanceProviderTest {

    @Autowired
    private DistanceMatrixProvider haversineDistanceProvider;

    @Test
    public void testEmpty() throws Exception {
        Coordinates from = new Coordinates(54.34d, 23.45d);
        int index = haversineDistanceProvider.nearest(from, new ArrayList<>());
        assertEquals(-1, index);
    }

    @Test
    public void testOneElement() throws Exception {
        Coordinates from = new Coordinates(54.34d, 23.45d);
        int index = haversineDistanceProvider.nearest(from, Arrays.asList(new Coordinates(52.45d, 27.34d)));
        assertEquals(0, index);
    }

    @Test
    public void testManyElems() throws Exception {
        Coordinates from = new Coordinates(54.34d, 23.45d);
        List<Coordinates> to =  Arrays.asList(
                new Coordinates(52.45d, 27.34d),
                new Coordinates(54.3299d, 23.42d),
                new Coordinates(51.43d, 22.14d),
                new Coordinates(52.41d, 26.24d),
                new Coordinates(53.15d, 27.20d),
                new Coordinates(54.33d, 23.42d),
                new Coordinates(51.00d, 23.44d),
                new Coordinates(52.46d, 27.37d),
                new Coordinates(54.327d, 23.41d),
                new Coordinates(51.422d, 22.00d),
                new Coordinates(52.401d, 26.222d),
                new Coordinates(53.199d, 27.11d),
                new Coordinates(54.301d, 23.201d),
                new Coordinates(51.001d, 23.339d)

        );
        int index = haversineDistanceProvider.nearest(from, to);
        assertEquals(5, index);
    }
}
