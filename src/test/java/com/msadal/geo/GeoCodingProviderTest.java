package com.msadal.geo;

import com.google.common.io.CharStreams;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.msadal.GeoServiceApp;
import com.msadal.domain.ShopAddress;
import com.msadal.domain.ShopInfo;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = GeoServiceApp.class)
public class GeoCodingProviderTest {

    @Autowired
    private GeoCodingProvider geoCodingProvider;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testCorrectAddress() throws Exception {
        Coordinates coordinates = geoCodingProvider.geoCode("384", "SE9 6UF");
        assertNotNull(coordinates);
        assertNotNull(coordinates.getLatitude());
        assertNotNull(coordinates.getLongitude());
    }

    @Test
    public void testEmptyPostCode() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage("Couldn't get coordinates for given post code ");
        geoCodingProvider.geoCode("384", "");
    }

    @Test
    public void testIncorrectPostCode() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage("Couldn't get coordinates for given post code XXX FFF");
        geoCodingProvider.geoCode("384", "XXX FFF");
    }

    @Test
    public void testIncorrectNumber() throws Exception {
        thrown.expect(Exception.class);
        thrown.expectMessage("Couldn't get coordinates for given address DFFF38412222, SE9 6UF");
        geoCodingProvider.geoCode("DFFF38412222", "SE9 6UF");
    }

    @Test
    public void testProcessMultiAccepted() throws Exception {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream is = classLoader.getResourceAsStream("shopsaccepted.csv");
        List<String> shopsAsJson = CharStreams.readLines(new InputStreamReader(is));
        Gson gson = new GsonBuilder().create();

        for (String json : shopsAsJson) {
            ShopInfo shopInfo = gson.fromJson(json, ShopInfo.class);
            ShopAddress address = shopInfo.getShopAddress();
            Coordinates c = geoCodingProvider.geoCode(address.getNumber(), address.getPostCode());
            assertNotNull(c);
        }
    }

    @Test
    public void testProcessMultiRejected() throws Exception {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream is = classLoader.getResourceAsStream("shopsrejected.csv");
        List<String> shopsAsJson = CharStreams.readLines(new InputStreamReader(is));
        Gson gson = new GsonBuilder().create();

        int rejectedCounter = 0;
        for (String json : shopsAsJson) {
            ShopInfo shopInfo = gson.fromJson(json, ShopInfo.class);
            ShopAddress address = shopInfo.getShopAddress();
            try {
                Coordinates c = geoCodingProvider.geoCode(address.getNumber(), address.getPostCode());
            } catch (Exception e) {
                ++rejectedCounter;
            }
        }
        assertEquals(shopsAsJson.size(), rejectedCounter);
    }
}
