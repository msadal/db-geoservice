package com.msadal.store;

import com.msadal.GeoServiceApp;
import com.msadal.domain.ShopInfo;
import com.msadal.domain.ShopRegisterTask;
import com.msadal.domain.Status;
import com.msadal.util.AbstractKeyFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = GeoServiceApp.class)
public class InMemoryArrayTaskStoreTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private Store<ShopRegisterTask> taskStore;

    @Autowired
    private AbstractKeyFactory sequenceKeyFactory;

    @Before
    public void setUp() {
        taskStore = new InMemoryArrayStore<>(ShopRegisterTask.class);
    }

    @Test
    public void testAddSuccess() throws Exception {
        ShopInfo shopInfo1 = new ShopInfo("Cost Cutter", "402", "SE10 7DF");
        ShopInfo shopInfo2 = new ShopInfo("Tesco Express", "403", "SE10 7DF");
        ShopRegisterTask task1 = new ShopRegisterTask(sequenceKeyFactory, shopInfo1, Status.NEW);
        ShopRegisterTask task2 = new ShopRegisterTask(sequenceKeyFactory, shopInfo2, Status.NEW);

        taskStore.add(task1);
        taskStore.add(task2);

        assertSame(task1, taskStore.get(task1.getKey()));
        assertSame(task2, taskStore.get(task2.getKey()));
        List<ShopRegisterTask> list = taskStore.asList();
        assertSame(task1, list.get(0));
        assertSame(task2, list.get(1));
        assertEquals(2, list.size());
    }

    @Test
    public void testEnsureCapacity() throws Exception {
        for (int i = 0; i < 18; ++i) {
            ShopInfo shopInfo = new ShopInfo("Blue Water shop " + i, "100", "SE10 7DF");
            ShopRegisterTask task = new ShopRegisterTask(sequenceKeyFactory, shopInfo, Status.NEW);
            taskStore.add(task);
        }

        List<ShopRegisterTask> list = taskStore.asList();
        for (int i = 0; i < 18; ++i) {
            assertNotNull(list.get(i));
        }
        assertEquals(18, list.size());
    }

    @Test()
    public void testAddNonUniqueElementExceptionThrown() throws Exception {
        ShopInfo shopInfo1 = new ShopInfo("Boots", "400", "SE10 7DF");
        ShopInfo shopInfo2 = new ShopInfo("Boots", "400", "SE10 7DF");

        AbstractKeyFactory mockFactory = Mockito.mock(AbstractKeyFactory.class);
        Mockito.when(mockFactory.createKey(any())).thenReturn("1");
        ShopRegisterTask task1 = new ShopRegisterTask(mockFactory, shopInfo1, Status.NEW);
        ShopRegisterTask task2 = new ShopRegisterTask(mockFactory, shopInfo2, Status.NEW);

        taskStore.add(task1);
        assertSame(task1, taskStore.get(task1.getKey()));

        thrown.expect(NonUniqueElementException.class);
        taskStore.add(task2);

    }

    @Test()
    public void testAddNonUniqueElementContents() throws Exception {
        ShopInfo shopInfo1 = new ShopInfo("Boots", "400", "SE10 7DF");
        ShopInfo shopInfo2 = new ShopInfo("Boots", "400", "SE10 7DF");

        AbstractKeyFactory mockFactory = Mockito.mock(AbstractKeyFactory.class);
        Mockito.when(mockFactory.createKey(any())).thenReturn("1");
        ShopRegisterTask task1 = new ShopRegisterTask(mockFactory, shopInfo1, Status.NEW);
        ShopRegisterTask task2 = new ShopRegisterTask(mockFactory, shopInfo2, Status.NEW);

        taskStore.add(task1);
        assertSame(task1, taskStore.get(task1.getKey()));

        try {
            taskStore.add(task2);
        } catch (NonUniqueElementException e) {
            // ignore
        }

        assertNotEquals(task2, taskStore.get(task2.getKey()));
        List<ShopRegisterTask> list = taskStore.asList();
        assertSame(task1, list.get(0));
        assertEquals(1, list.size());
    }

    @Test
    public void testAsListEmpty() throws Exception {
        List<ShopRegisterTask> list = taskStore.asList();
        assertTrue(list.isEmpty());
    }

    @Test
    public void testUpdateSuccess() throws Exception {
        ShopInfo shopInfo1 = new ShopInfo("Boots", "400", "SE10 7DF");

        AbstractKeyFactory mockFactory = Mockito.mock(AbstractKeyFactory.class);
        Mockito.when(mockFactory.createKey(any())).thenReturn("1");
        ShopRegisterTask task = new ShopRegisterTask(mockFactory, shopInfo1, Status.NEW);
        ShopRegisterTask taskUpdated = new ShopRegisterTask(task, Status.OK);

        taskStore.add(task);
        assertSame(task, taskStore.get(task.getKey()));
        assertEquals(Status.NEW, taskStore.get(task.getKey()).getStatus());

        taskStore.update(taskUpdated);
        assertSame(taskUpdated, taskStore.get(task.getKey()));
        assertEquals(Status.OK, taskStore.get(task.getKey()).getStatus());
    }

    @Test(expected = NonExistingElementException.class)
    public void testUpdateNonExistingElementThrown() throws Exception {
        ShopInfo shopInfo1 = new ShopInfo("Boots", "400", "SE10 7DF");

        AbstractKeyFactory mockFactory = Mockito.mock(AbstractKeyFactory.class);
        Mockito.when(mockFactory.createKey(any())).thenReturn("10");
        ShopRegisterTask task = new ShopRegisterTask(mockFactory, shopInfo1, Status.NEW);

        taskStore.update(task);
    }

    @Test
    public void testConcurrentAdd() throws Exception {
        ExecutorService executor = Executors.newFixedThreadPool(8);
        CountDownLatch countDownLatch = new CountDownLatch(1);
        for (int i = 0; i < 50; ++i) {
            executor.submit(() -> {
                try {
                    countDownLatch.await();
                    ShopInfo shopInfo = new ShopInfo("Blue Water shop ", "100", "SE10 7DF");
                    ShopRegisterTask task = new ShopRegisterTask(sequenceKeyFactory, shopInfo, Status.NEW);
                    taskStore.add(task);
                } catch (Exception e) {
                    // ignore
                }
            });
        }
        executor.shutdown();
        countDownLatch.countDown();
        while (!executor.awaitTermination(1, TimeUnit.SECONDS));

        List<ShopRegisterTask> list = taskStore.asList();
        for (int i = 0; i < 50; ++i) {
            assertNotNull(list.get(i));
        }
        assertEquals(50, list.size());
    }
}
