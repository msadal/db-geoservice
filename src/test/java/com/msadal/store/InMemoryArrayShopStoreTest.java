package com.msadal.store;

import com.msadal.GeoServiceApp;
import com.msadal.domain.Shop;
import com.msadal.domain.ShopInfo;
import com.msadal.util.AbstractKeyFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = GeoServiceApp.class)
public class InMemoryArrayShopStoreTest {

    private Store<Shop> shopStore;

    @Autowired
    private AbstractKeyFactory md5HashKeyFactory;

    @Before
    public void setUp() {
        shopStore = new InMemoryArrayStore<>(Shop.class);
    }

    @Test
    public void testAdd() throws Exception {
        ShopInfo shopInfo1 = new ShopInfo("Cost Cutter", "402", "SE10 7DF");
        ShopInfo shopInfo2 = new ShopInfo("Tesco Express", "403", "SE10 7DF");
        Shop shop1 = new Shop(md5HashKeyFactory, shopInfo1, 0d, 0d);
        Shop shop2 = new Shop(md5HashKeyFactory, shopInfo2, 1d, 1d);

        shopStore.add(shop1);
        shopStore.add(shop2);

        assertSame(shop1, shopStore.get(shop1.getKey()));
        assertSame(shop2, shopStore.get(shop2.getKey()));
        List<Shop> list = shopStore.asList();
        assertSame(shop1, list.get(0));
        assertSame(shop2, list.get(1));
        assertEquals(2, list.size());
    }

    @Test(expected = NonUniqueElementException.class)
    public void testNonUniqueElementExceptionThrown() throws Exception {
        ShopInfo shopInfo1 = new ShopInfo("Boots", "400", "SE10 7DF");
        ShopInfo shopInfo2 = new ShopInfo("Boots", "400", "SE10 7DF");

        Shop shop1 = new Shop(md5HashKeyFactory, shopInfo1, 0d, 0d);
        Shop shop2 = new Shop(md5HashKeyFactory, shopInfo2, 1d, 1d);

        shopStore.add(shop1);
        shopStore.add(shop2);
    }

    @Test
    public void testNonUniqueElementContents() throws Exception {
        ShopInfo shopInfo1 = new ShopInfo("Boots", "400", "SE10 7DF");
        ShopInfo shopInfo2 = new ShopInfo("Boots", "400", "SE10 7DF");

        Shop shop1 = new Shop(md5HashKeyFactory, shopInfo1, 0d, 0d);
        Shop shop2 = new Shop(md5HashKeyFactory, shopInfo2, 1d, 1d);

        shopStore.add(shop1);
        try {
            shopStore.add(shop2);
        } catch (NonUniqueElementException e) {
            // ignore
        }

        assertSame(shop1, shopStore.get(shop1.getKey()));
        assertNotSame(shop2, shopStore.get(shop2.getKey()));
        List<Shop> list = shopStore.asList();
        assertFalse(list.isEmpty());
        assertSame(shop1, list.get(0));
        assertEquals(1, list.size());
    }
}
