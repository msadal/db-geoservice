package com.msadal;

import com.google.common.io.CharStreams;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.msadal.domain.ShopInfo;
import com.msadal.domain.ShopRegisterTask;
import com.msadal.domain.Status;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = GeoServiceApp.class)
@WebAppConfiguration()
@IntegrationTest({"server.port:8090"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GeoAppIntegrationTest {

    @Value("${local.server.port}")
    int port;

    @Before
    public void setUp() throws Exception {
        RestAssured.port = port;
    }

    @Test
    public void ATestRegisterAccepted() throws Exception {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream is = classLoader.getResourceAsStream("shopsaccepted.csv");
        List<String> shopsAsJson = CharStreams.readLines(new InputStreamReader(is));

        List<String> keys = new LinkedList<>();
        shopsAsJson.subList(0, 19).forEach(json -> {
            String key = given()
                    .contentType(ContentType.JSON)
                    .body(json)
                    .when()
                    .post("/register")
                    .then()
                    .statusCode(HttpStatus.OK.value())
                    .body("status", equalTo(Status.NEW.name()))
                    .extract()
                    .path("key");
            keys.add(key);
        });

        Thread.sleep(TimeUnit.SECONDS.toMillis(20));

        for (String key : keys) {
            given()
                    .when()
                    .get("/task/" + key)
                    .then()
                    .statusCode(HttpStatus.OK.value())
                    .body("key", equalTo(key))
                    .body("status", equalTo(Status.OK.name()));
        }
    }

    @Test
    public void BTestRegisterRejected() throws Exception {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream is = classLoader.getResourceAsStream("shopsrejected.csv");
        List<String> shopsAsJson = CharStreams.readLines(new InputStreamReader(is));

        List<String> keys = new LinkedList<>();
        shopsAsJson.forEach(json -> {
            String key = given()
                    .contentType(ContentType.JSON)
                    .body(json)
                    .when()
                    .post("/register")
                    .then()
                    .statusCode(HttpStatus.OK.value())
                    .body("status", equalTo(Status.NEW.name()))
                    .extract()
                    .path("key");
            keys.add(key);
        });

        Thread.sleep(TimeUnit.SECONDS.toMillis(10));

        for (String key : keys) {
            given()
                    .when()
                    .get("/task/" + key)
                    .then()
                    .statusCode(HttpStatus.OK.value())
                    .body("key", equalTo(key))
                    .body("status", equalTo(Status.REJECTED.name()));
        }
    }

    @Test
    public void ATestRegisterDuplicate() throws Exception {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream is = classLoader.getResourceAsStream("shopsaccepted.csv");
        List<String> shopsAsJson = CharStreams.readLines(new InputStreamReader(is)).subList(20, 22);

        // add tasks
        shopsAsJson.forEach(json -> {
            given()
                    .contentType(ContentType.JSON)
                    .body(json)
                    .when()
                    .post("/register")
                    .then()
                    .statusCode(HttpStatus.OK.value())
                    .body("status", equalTo(Status.NEW.name()));
        });

        Thread.sleep(TimeUnit.SECONDS.toMillis(5));

        // add duplicate tasks
        List<String> keys = new LinkedList<>();
        shopsAsJson.forEach(json -> {
            String key = given()
                    .contentType(ContentType.JSON)
                    .body(json)
                    .when()
                    .post("/register")
                    .then()
                    .statusCode(HttpStatus.OK.value())
                    .body("status", equalTo(Status.NEW.name()))
                    .extract()
                    .path("key");
            keys.add(key);
        });

        Thread.sleep(TimeUnit.SECONDS.toMillis(5));

        for (String key : keys) {
            given()
                    .when()
                    .get("/task/" + key)
                    .then()
                    .statusCode(HttpStatus.OK.value())
                    .body("key", equalTo(key))
                    .body("status", equalTo(Status.DUPLICATE.name()));
        }
    }

    @Test
    public void DTestSearchNearestShop() throws Exception {
        double longitude=-0.093d;
        double latitude=51.51506d;

        given()
                .param("lat", latitude)
                .param("lng", longitude)
                .when()
                .get("/search")
                .then()
                .body("key", equalTo("b00f7483490e573710161505f8ccc8c7"))
                .body("shopName", equalTo("Bespoke Cycling"))
                .body("shopAddress.number", equalTo("30"))
                .body("shopAddress.postCode", equalTo("EC2V 7PG"));

        longitude=-0.2053d;
        latitude=51.5159d;
        given()
                .param("lat", latitude)
                .param("lng", longitude)
                .when()
                .get("/search")
                .then()
                .body("key", equalTo("057155b4f9eebd50e858601c1f23ea8e"));

        longitude=-0.1226d;
        latitude=51.5120d;
        given()
                .param("lat", latitude)
                .param("lng", longitude)
                .when()
                .get("/search")
                .then()
                .body("key", equalTo("ed435f86d5700ae30219c973181b6d95"));
    }
}
