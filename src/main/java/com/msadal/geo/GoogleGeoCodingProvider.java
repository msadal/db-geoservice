package com.msadal.geo;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.AddressType;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import static com.google.maps.model.ComponentFilter.country;
import static com.google.maps.model.ComponentFilter.postalCode;
import static com.google.maps.model.LocationType.ROOFTOP;

public class GoogleGeoCodingProvider implements GeoCodingProvider {

    @Value("${geo.api.country}")
    private String country;

    @Autowired
    private GeoApiContext context;

    @Override
    public Coordinates geoCode(String number, String postalCode) throws Exception {
        // get the coordinates for postal code na country
        LatLng location = getPostalCodeCoordinates(postalCode);

        // get street address for given coordinates
        String streetName = getStreetNameForCoordinates(location);
        String address = number + " " + streetName;
        GeocodingResult[] finalResults = GeocodingApi.newRequest(context)
                .address(address)
                .components(postalCode(postalCode), country(country))
                .await();
        if (finalResults != null && finalResults.length > 0 &&
                (finalResults[0].addressComponents[0].shortName.equalsIgnoreCase(number) ||
                        finalResults[0].addressComponents[0].longName.equalsIgnoreCase(number))) {
            LatLng l = finalResults[0].geometry.location;
            return new Coordinates(l.lat, l.lng);
        }
        throw new Exception("Couldn't get coordinates for given address " + number + ", " + postalCode);
    }

    private String getStreetNameForCoordinates(LatLng location) throws Exception {
        GeocodingResult[] streetResults = GeocodingApi.newRequest(context)
                .latlng(location)
                .locationType(ROOFTOP)
                .resultType(AddressType.STREET_ADDRESS)
                .await();

        if (streetResults != null && streetResults.length > 0) {
            // get street from found address
            String streetName = null;
            if (streetResults[0].types[0].equals(AddressType.STREET_ADDRESS) && streetResults[0].addressComponents[1].longName != null) {
                return streetResults[0].addressComponents[1].longName;
            } else if (streetResults[0].types[0].equals(AddressType.ROUTE)) {
                return streetResults[0].addressComponents[0].longName;
            }
        }
        throw new Exception("Couldn't get street name for given coordinates " + location.toString());
    }

    private LatLng getPostalCodeCoordinates(String postCode) throws Exception {
        GeocodingResult[] postalCodeResults = GeocodingApi.newRequest(context)
                .components(postalCode(postCode), country(country))
                .await();

        if (postalCodeResults != null && postalCodeResults.length > 0 &&
                (postalCodeResults[0].addressComponents[0].shortName.equalsIgnoreCase(postCode) ||
                        postalCodeResults[0].addressComponents[0].longName.equalsIgnoreCase(postCode))) {
            return postalCodeResults[0].geometry.location;
        }
        throw new Exception("Couldn't get coordinates for given post code " + postCode);
    }
}
