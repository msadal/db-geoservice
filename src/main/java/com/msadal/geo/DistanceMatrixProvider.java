package com.msadal.geo;


import com.msadal.domain.Shop;

import java.util.List;

public interface DistanceMatrixProvider {
    int nearest(Coordinates from, List<Coordinates> list) throws Exception;
}
