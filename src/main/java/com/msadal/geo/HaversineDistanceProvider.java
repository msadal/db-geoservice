package com.msadal.geo;

import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HaversineDistanceProvider implements DistanceMatrixProvider {

    private final static int RADIUS = 6371;
    private final static int MAX_THREADS = 8;

    private static class IndexDistance {
        private int index;
        private long distance;

        public IndexDistance(int index, long distance) {
            this.index = index;
            this.distance = distance;
        }

        public int getIndex() {
            return index;
        }

        public long getDistance() {
            return distance;
        }
    }

    @Override
    public int nearest(final Coordinates from, List<Coordinates> list) throws Exception {
        if (list.size() == 0) {
            return -1;
        } else if (list.size() == 1) {
            return 0;
        }

        int threads = list.size() / 4 > MAX_THREADS ? MAX_THREADS: Math.max(list.size() / 4, 1);
        ExecutorService executorService = Executors.newFixedThreadPool(threads);
        CompletionService<IndexDistance> completionService = new ExecutorCompletionService<>(executorService);
        for (int i = 0; i < list.size(); ++i) {
            Coordinates to = list.get(i);
            final int index = i;
            completionService.submit(() -> new IndexDistance(index, distance(from, to)));
        }
        executorService.shutdown();

        int minDistanceIndex = 0;
        long minDistance = Long.MAX_VALUE;
        for (int i = 0; i < list.size(); ++i) {
            IndexDistance result = completionService.take().get();
            if (i == 0) {
                minDistanceIndex = result.getIndex();
                minDistance = result.getDistance();
                continue;
            }
            if (minDistance > result.getDistance()) {
                minDistanceIndex = result.getIndex();
                minDistance = result.getDistance();
            }
        }
        return minDistanceIndex;
    }

    public long distance(Coordinates from, Coordinates to) {
        Double latDistance = Math.toRadians(to.getLatitude() - from.getLatitude());
        Double lonDistance = Math.toRadians(to.getLongitude() - from.getLongitude());
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(from.getLatitude())) * Math.cos(Math.toRadians(to.getLatitude()))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = RADIUS * c * 1000; // convert to meters
        return new Double(distance).longValue();
    }
}
