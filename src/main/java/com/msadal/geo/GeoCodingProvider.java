package com.msadal.geo;

public interface GeoCodingProvider {
    Coordinates geoCode(String number, String postalCode) throws Exception;
}
