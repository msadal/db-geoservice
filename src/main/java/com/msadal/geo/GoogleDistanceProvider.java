package com.msadal.geo;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.LatLng;
import com.google.maps.model.Unit;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedList;
import java.util.List;

public class GoogleDistanceProvider implements DistanceMatrixProvider {

    @Autowired
    private GeoApiContext context;

    @Override
    public int nearest(Coordinates from, List<Coordinates> list) throws Exception {
        if (list.size() == 0) {
            return -1;
        } else if (list.size() == 1) {
            return 0;
        }

        LatLng fromLatLng = new LatLng(from.getLatitude(), from.getLongitude());
        List<LatLng> toLatLngList = new LinkedList<>();
        list.forEach(c -> {
            toLatLngList.add(new LatLng(c.getLatitude(), c.getLongitude()));
        });

        DistanceMatrix matrix = DistanceMatrixApi.newRequest(context)
                .origins(fromLatLng)
                .destinations(toLatLngList.toArray(new LatLng[] {}))
                .units(Unit.METRIC)
                .await();

        int minDistanceIndex = 0;
        long minDistance = Long.MAX_VALUE;
        for (int i = 0; i < matrix.rows[0].elements.length; ++i) {
            if (i == 0) {
                minDistance = matrix.rows[0].elements[0].distance.inMeters;
                continue;
            }
            if (minDistance > matrix.rows[0].elements[i].distance.inMeters) {
                minDistanceIndex = i;
                minDistance = matrix.rows[0].elements[i].distance.inMeters;
            }
        }
        return minDistanceIndex;
    }
}
