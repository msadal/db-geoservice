package com.msadal;

import com.msadal.domain.Shop;
import com.msadal.domain.ShopInfo;
import com.msadal.domain.ShopRegisterTask;
import com.msadal.geo.Coordinates;
import com.msadal.store.NonUniqueElementException;
import com.msadal.util.LatLngValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;


@RestController()
public class ShopResourceController {

    @Autowired
    private ShopManager shopManager;

    @RequestMapping(value="/register", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ShopInfo> register(@RequestBody ShopInfo shopInfo) {
        if (StringUtils.isEmpty(shopInfo.getShopName()) ||
                shopInfo.getShopAddress() == null ||
                StringUtils.isEmpty(shopInfo.getShopAddress().getNumber()) ||
                StringUtils.isEmpty(shopInfo.getShopAddress().getPostCode())) {
            return new ResponseEntity<ShopInfo>(shopInfo, HttpStatus.BAD_REQUEST);
        }
        try {
            ShopRegisterTask registerRequest = shopManager.addRegisterTask(shopInfo);
            return new ResponseEntity<ShopInfo>(registerRequest, HttpStatus.OK);
        } catch (NonUniqueElementException e) {
            return new ResponseEntity<ShopInfo>((ShopInfo) null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/task/{key}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ShopRegisterTask> getTask(@PathVariable("key") String key) {
        ShopRegisterTask registerRequest = shopManager.getRegisterTask(key);
        HttpStatus status = registerRequest != null? HttpStatus.OK: HttpStatus.NOT_FOUND;
        return new ResponseEntity<ShopRegisterTask>(registerRequest, status);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Shop> searchShop(@RequestParam("lat") String lat, @RequestParam("lng") String lng) {
        Coordinates coordinates = LatLngValidator.coordinates(lat, lng);
        if (coordinates == null) {
            return new ResponseEntity<Shop>((Shop) null, HttpStatus.BAD_REQUEST);
        }
        try {
            Shop nearestShop = shopManager.searchShop(coordinates);
            HttpStatus status = nearestShop != null? HttpStatus.OK: HttpStatus.NOT_FOUND;
            return new ResponseEntity<Shop>(nearestShop, status);
        } catch (Exception e) {
            return new ResponseEntity<Shop>((Shop) null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
