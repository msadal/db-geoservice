package com.msadal;

import com.google.maps.GeoApiContext;
import com.msadal.domain.Shop;
import com.msadal.domain.ShopRegisterTask;
import com.msadal.geo.DistanceMatrixProvider;
import com.msadal.geo.GeoCodingProvider;
import com.msadal.geo.GoogleGeoCodingProvider;
import com.msadal.geo.HaversineDistanceProvider;
import com.msadal.store.InMemoryArrayStore;
import com.msadal.store.Store;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class AppConfig {

    @Value("${geo.api.key}")
    private String apiKey;

    @Bean
    public GeoApiContext getGeoApiContext() {
        return new GeoApiContext()
                .setApiKey(apiKey)
                .setQueryRateLimit(3)
                .setReadTimeout(1, TimeUnit.SECONDS)
                .setConnectTimeout(1, TimeUnit.SECONDS);
    }

    @Bean
    public GeoCodingProvider getGeoCodingProvider() {
        return new GoogleGeoCodingProvider();
    }

    @Bean
    public Store<Shop> shopStore() {
        return new InMemoryArrayStore(Shop.class);
    }

    @Bean
    public Store<ShopRegisterTask> taskStore() {
        return new InMemoryArrayStore<>(ShopRegisterTask.class);
    }

    @Bean
    public DistanceMatrixProvider distanceMatrixProvider() {
        return new HaversineDistanceProvider();
    }
}
