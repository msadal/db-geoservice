package com.msadal.util;

import com.msadal.geo.Coordinates;
import org.springframework.util.StringUtils;

public class LatLngValidator {

    public static Coordinates coordinates(String lat, String lng) {
        Double latitude = null;
        Double longitude = null;
        if (!StringUtils.isEmpty(lat) && !StringUtils.isEmpty(lng)) {
            try {
                latitude = Double.parseDouble(lat);
                longitude = Double.parseDouble(lng);
            } catch (NumberFormatException e) {
                // handle later
            }
        }
        if ((latitude != null && longitude != null &&
                longitude.compareTo(-180d) != -1 && longitude.compareTo(180d) != 1 &&
                latitude.compareTo(-85.05115d) != -1 && latitude.compareTo(85d) != 1)) {
            return new Coordinates(latitude, longitude);
        }
        return null;
    }
}
