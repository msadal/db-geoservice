package com.msadal.util;

import com.msadal.domain.ShopInfo;

public interface AbstractKeyFactory {
    String createKey(ShopInfo shopInfo);
}
