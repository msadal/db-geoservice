package com.msadal.util;

import com.msadal.domain.ShopInfo;
import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Component
public class Md5HashKeyFactory implements AbstractKeyFactory {

    private final static char DELIMITER = '_';

    public String createKey(ShopInfo shopInfo) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            StringBuilder feed = new StringBuilder(shopInfo.getShopName()).append(DELIMITER)
                    .append(shopInfo.getShopAddress().getNumber()).append(DELIMITER)
                    .append(shopInfo.getShopAddress().getPostCode());

            md.update(feed.toString().getBytes());
            byte[] digest = md.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Exception while creating md5 key", e);
        }
    }
}
