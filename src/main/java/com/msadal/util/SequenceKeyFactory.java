package com.msadal.util;

import com.msadal.domain.ShopInfo;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Component
public class SequenceKeyFactory implements AbstractKeyFactory {
    private AtomicInteger sequence = new AtomicInteger(0);

    public String createKey(ShopInfo shopInfo) {
        return sequence.incrementAndGet() + "";
    }
}
