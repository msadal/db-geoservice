package com.msadal;

import com.msadal.domain.Shop;
import com.msadal.domain.ShopInfo;
import com.msadal.domain.ShopRegisterTask;
import com.msadal.geo.Coordinates;
import com.msadal.store.NonUniqueElementException;

public interface ShopManager {
    ShopRegisterTask addRegisterTask(ShopInfo shopInfo) throws NonUniqueElementException;
    ShopRegisterTask getRegisterTask(String key);
    Shop searchShop(Coordinates coordinates) throws Exception;
    void shutdown();
}
