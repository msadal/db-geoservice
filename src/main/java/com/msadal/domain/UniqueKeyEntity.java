package com.msadal.domain;

public interface UniqueKeyEntity {
    String getKey();
}
