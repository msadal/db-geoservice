package com.msadal.domain;


public class ShopAddress {
    private String number;
    private String postCode;

    public ShopAddress() {
    }

    public ShopAddress(String number, String postCode) {
        this.number = number;
        this.postCode = postCode;
    }

    public String getNumber() {
        return number;
    }

    public String getPostCode() {
        return postCode;
    }

    @Override
    public String toString() {
        return "{" +
                "number='" + number + '\'' +
                ", postCode='" + postCode + '\'' +
                '}';
    }
}
