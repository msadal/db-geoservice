package com.msadal.domain;

public class ShopInfo {
    private String shopName;
    private ShopAddress shopAddress;

    public ShopInfo() {
    }

    public ShopInfo(String shopName, String number, String postCode) {
        this.shopName = shopName;
        this.shopAddress = new ShopAddress(number, postCode);
    }

    public ShopInfo(ShopInfo shopBase) {
        this.shopName = shopBase.getShopName();
        this.shopAddress = shopBase.getShopAddress();
    }

    public String getShopName() {
        return shopName;
    }

    public ShopAddress getShopAddress() {
        return shopAddress;
    }

    @Override
    public String toString() {
        return "{" +
                "shopName='" + shopName + '\'' +
                ", shopAddress=" + shopAddress +
                '}';
    }
}
