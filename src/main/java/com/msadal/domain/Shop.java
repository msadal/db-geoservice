package com.msadal.domain;


import com.msadal.util.AbstractKeyFactory;

public class Shop extends ShopInfo implements UniqueKeyEntity {
    private String key;
    private double shopLongitude;
    private double shopLatitude;

    public Shop() {
    }

    public Shop(AbstractKeyFactory keyFactory, ShopInfo shopBase, Double shopLongitude, Double shopLatitude) {
        super(shopBase);
        this.key = keyFactory.createKey(shopBase);
        this.shopLongitude = shopLongitude;
        this.shopLatitude = shopLatitude;
    }

    public String getKey() {
        return key;
    }

    public Double getShopLongitude() {
        return shopLongitude;
    }

    public Double getShopLatitude() {
        return shopLatitude;
    }

    @Override
    public String toString() {
        return "{" +
                "key='" + key + '\'' +
                ", shopLongitude=" + shopLongitude +
                ", shopLatitude=" + shopLatitude +
                '}';
    }
}
