package com.msadal.domain;

import com.msadal.util.AbstractKeyFactory;

public class ShopRegisterTask extends ShopInfo implements UniqueKeyEntity {
    private String key;
    private Status status;

    public ShopRegisterTask() {
    }

    public ShopRegisterTask(AbstractKeyFactory keyFactory, ShopInfo shopBase, Status status) {
        super(shopBase);
        this.key = keyFactory.createKey(shopBase);
        this.status = status;
    }

    public ShopRegisterTask(ShopRegisterTask oldTask, Status newStatus) {
        super(oldTask);
        this.key = oldTask.getKey();
        this.status = newStatus;
    }

    public String getKey() {
        return key;
    }

    public Status getStatus() {
        return status;
    }
}
