package com.msadal.domain;

public enum Status {
    NEW,
    OK,
    DUPLICATE,
    REJECTED
}
