package com.msadal;

import com.google.common.util.concurrent.*;
import com.msadal.domain.Shop;
import com.msadal.domain.ShopInfo;
import com.msadal.domain.ShopRegisterTask;
import com.msadal.domain.Status;
import com.msadal.geo.Coordinates;
import com.msadal.geo.DistanceMatrixProvider;
import com.msadal.geo.GeoCodingProvider;
import com.msadal.store.NonExistingElementException;
import com.msadal.store.NonUniqueElementException;
import com.msadal.store.Store;
import com.msadal.util.AbstractKeyFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Component
public class ShopManagerImpl implements ShopManager {

    static Logger log = Logger.getLogger(ShopManagerImpl.class);

    private final static int GEOCODING_THREADS = 20;

    @Autowired
    private AbstractKeyFactory md5HashKeyFactory;

    @Autowired
    private Store<Shop> shopStore;

    @Autowired
    private AbstractKeyFactory sequenceKeyFactory;

    @Autowired
    private Store<ShopRegisterTask> taskStore;

    @Autowired
    private GeoCodingProvider geoCodingProvider;

    @Autowired
    private DistanceMatrixProvider distanceMatrixProvider;

    ListeningExecutorService geoCodingExecutor;

    public ShopManagerImpl() {
        this.geoCodingExecutor = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(GEOCODING_THREADS));
    }

    @Override
    public ShopRegisterTask addRegisterTask(ShopInfo shopInfo) throws NonUniqueElementException {
        ShopRegisterTask task = new ShopRegisterTask(sequenceKeyFactory, shopInfo, Status.NEW);
        taskStore.add(task);
        log.info("Added new task [" + task.getKey() + "] shop = " + task.toString());

        String number = task.getShopAddress().getNumber();
        String postalCode = task.getShopAddress().getPostCode();
        ListenableFuture<Coordinates> future = geoCodingExecutor.submit(() -> geoCodingProvider.geoCode(number, postalCode));
        Futures.addCallback(future, new FutureCallback<Coordinates>() {
            @Override
            public void onSuccess(Coordinates c) {
                addNewShop(task, c);
            }

            @Override
            public void onFailure(Throwable throwable) {
                updateTaskStatusQuietly(task, Status.REJECTED);
            }
        });
        return task;
    }

    private void updateTaskStatusQuietly(ShopRegisterTask oldTask, Status newStatus) {
        try {
            log.info("Updating status task [" + oldTask.getKey() + "] status = " + newStatus.name());
            taskStore.update(new ShopRegisterTask(oldTask, newStatus));
        } catch (NonExistingElementException e) {
            log.error("Illegal application state - " + e.toString(), e);
        }
    }

    private void addNewShop(ShopRegisterTask task, Coordinates c) {
        try {
            Shop shop = new Shop(md5HashKeyFactory, task, c.getLongitude(), c.getLatitude());
            shopStore.add(shop);
            log.info("Added new shop for task [" + task.getKey() + "], shop = " + shop);
            updateTaskStatusQuietly(task, Status.OK);
        } catch (NonUniqueElementException e) {
            log.info("NonUniqueElementException - " + e.getMessage());
            updateTaskStatusQuietly(task, Status.DUPLICATE);
        }
    }

    @Override
    public ShopRegisterTask getRegisterTask(String key) {
        return taskStore.get(key);
    }

    @Override
    public Shop searchShop(Coordinates coordinates) throws Exception {
        List<Shop> list = shopStore.asList();
        if (list.isEmpty()) {
            return null;
        } else if (list.size() == 1) {
            return list.get(0);
        }
        List<Coordinates> shopsCoordinates = list.stream()
                .map(shop -> new Coordinates(shop.getShopLatitude(), shop.getShopLongitude()))
                .collect(Collectors.toList());
        int minDistanceIndex = distanceMatrixProvider.nearest(coordinates, shopsCoordinates);
        return list.get(minDistanceIndex);
    }

    @Override
    @PreDestroy
    public void shutdown() {
        log.info("Shutting down ShopManager");
        geoCodingExecutor.shutdown();
    }
}
