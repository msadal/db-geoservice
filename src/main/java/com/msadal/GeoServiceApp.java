package com.msadal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeoServiceApp {
    public static void main(String[] args) {
        SpringApplication.run(GeoServiceApp.class, args);
    }
}