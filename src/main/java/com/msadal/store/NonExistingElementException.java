package com.msadal.store;

public class NonExistingElementException extends Exception {

    private String key;

    public NonExistingElementException(String key) {
        super("Update failed, element with given key doesn't exists in store [key = " + key + "]");
        this.key = key;
    }
}
