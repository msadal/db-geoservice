package com.msadal.store;

import com.google.common.collect.ImmutableList;
import com.msadal.domain.UniqueKeyEntity;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class InMemoryArrayStore<T extends UniqueKeyEntity> implements Store<T> {

    private final static int INITIAL_CAPACITY = 16;
    private ReadWriteLock lock;
    private T[] array;
    private int elemCounter;

    public InMemoryArrayStore(Class<T> clazz) {
        this.lock = new ReentrantReadWriteLock();
        this.array = (T[]) Array.newInstance(clazz, INITIAL_CAPACITY);
    }

    public void add(T newElem) throws NonUniqueElementException {
        Lock writeLock = lock.writeLock();
        writeLock.lock();
        try {
            for (int i = 0; i < elemCounter; ++i) {
                if (array[i].getKey().equals(newElem.getKey())) {
                    throw new NonUniqueElementException(newElem.getKey());
                }
            }
            ensureCapacity();
            array[elemCounter++] = newElem;
        } finally {
            if (writeLock != null) {
                writeLock.unlock();
            }
        }
    }

    @Override
    public void update(T elem) throws NonExistingElementException {
        Lock writeLock = lock.writeLock();
        writeLock.lock();
        try {
            for (int i = 0; i < elemCounter; ++i) {
                if (array[i].getKey().equals(elem.getKey())) {
                    array[i] = elem;
                    return;
                }
            }
            throw new NonExistingElementException(elem.getKey());
        } finally {
            if (writeLock != null) {
                writeLock.unlock();
            }
        }
    }

    private void ensureCapacity() {
        if (elemCounter == array.length) {
            if (Integer.MAX_VALUE == elemCounter) {
                throw new OutOfMemoryError();
            }
            int newCapacity = 2 * elemCounter > Integer.MAX_VALUE? Integer.MAX_VALUE: 2 * elemCounter;
            array = Arrays.copyOf(array, newCapacity);
        }
    }

    public List<T> asList() {
        Lock readLock = lock.readLock();
        readLock.lock();
        try {
            return ImmutableList.copyOf(Arrays.copyOf(array, elemCounter));
        } finally {
            if (readLock != null) {
                readLock.unlock();
            }
        }
    }

    public T get(String key) {
        Lock readLock = lock.readLock();
        readLock.lock();
        try {
            for (int i = 0; i < elemCounter; ++i) {
                if (array[i].getKey().equals(key)) {
                    return array[i];
                }
            }
            return null;
        } finally {
            if (readLock != null) {
                readLock.unlock();
            }
        }
    }
}
