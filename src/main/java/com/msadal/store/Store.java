package com.msadal.store;

import com.msadal.domain.UniqueKeyEntity;

import java.util.Iterator;
import java.util.List;

public interface Store<T extends UniqueKeyEntity> {
    void add(T newElem) throws NonUniqueElementException;
    void update(T elem) throws NonExistingElementException;
    List<T> asList();
    T get(String key);
}
