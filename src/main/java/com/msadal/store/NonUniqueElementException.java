package com.msadal.store;

public class NonUniqueElementException extends Exception {
    private String key;

    public NonUniqueElementException(String key) {
        super("Element with given key already exists in store [key = " + key + "]");
        this.key = key;
    }
}
